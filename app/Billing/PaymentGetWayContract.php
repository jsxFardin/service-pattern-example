<?php

namespace App\Billing;

interface PaymentGetWayContract{
    public function setDiscount($discount);
    public function charge($amount);
}