<?php

namespace App\Providers;

use App\Billing\BankPaymentGetWay;
use App\Billing\CreditPaymentGetWay;
use App\Billing\PaymentGetWayContract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PaymentGetWayContract::class, function($app){

            if(request()->has('credit')) return new CreditPaymentGetWay('usd');
            
            return new BankPaymentGetWay('usd');
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
