<?php

namespace App\Http\Controllers;

use App\Billing\BankPaymentGetWay;
use App\Billing\PaymentGetWayContract;
use App\Orders\OrderDetails;
use Illuminate\Http\Request;

class PayOrderController extends Controller
{
    public function store(PaymentGetWayContract $paymentGetWay, OrderDetails $orderDetails){
        $order = $orderDetails->all();
        dd($paymentGetWay->charge(1000));
    }
}
